From e39c175af161607cbfd027fdff869f9564076f90 Mon Sep 17 00:00:00 2001
From: David Barbion <davidb@230ruedubac.fr>
Date: Sat, 30 Sep 2023 00:12:46 +0200
Subject: [PATCH 2/2] odroidgoa kmsdrm and rotate cursor

---
 src/video/kmsdrm/SDL_kmsdrmmouse.c | 39 ++++++++++++++++++++++++------
 1 file changed, 31 insertions(+), 8 deletions(-)

diff --git a/src/video/kmsdrm/SDL_kmsdrmmouse.c b/src/video/kmsdrm/SDL_kmsdrmmouse.c
index 99b869939..6bdbc8be8 100644
--- a/src/video/kmsdrm/SDL_kmsdrmmouse.c
+++ b/src/video/kmsdrm/SDL_kmsdrmmouse.c
@@ -39,6 +39,7 @@ static void KMSDRM_MoveCursor(SDL_Cursor * cursor);
 static void KMSDRM_FreeCursor(SDL_Cursor * cursor);
 static void KMSDRM_WarpMouse(SDL_Window * window, int x, int y);
 static int KMSDRM_WarpMouseGlobal(int x, int y);
+extern bool odroidgo_rotate;
 
 /**************************************************************************************/
 /* BEFORE CODING ANYTHING MOUSE/CURSOR RELATED, REMEMBER THIS.                        */
@@ -156,6 +157,7 @@ KMSDRM_DumpCursorToBO(SDL_VideoDisplay *display, SDL_Cursor *cursor)
     uint8_t *src_row;
 
     int i;
+    int j;
     int ret;
 
     if (!curdata || !dispdata->cursor_bo) {
@@ -176,8 +178,15 @@ KMSDRM_DumpCursorToBO(SDL_VideoDisplay *display, SDL_Cursor *cursor)
 
     /* Copy from the cursor buffer to a buffer that we can dump to the GBM BO. */
     for (i = 0; i < curdata->h; i++) {
-        src_row = &((uint8_t*)curdata->buffer)[i * curdata->w * 4];
-        SDL_memcpy(ready_buffer + (i * bo_stride), src_row, 4 * curdata->w);
+        if (odroidgo_rotate) {
+            for (j = 0; j < curdata->w; j++) {
+                src_row = ((uint32_t*)curdata->buffer)[i * curdata->w + j];
+                SDL_memcpy(ready_buffer + ((curdata->w - j + 1) * bo_stride) + i, &src_row, 4);
+            }
+        }else {
+            src_row = &((uint8_t*)curdata->buffer)[i * curdata->w * 4];
+            SDL_memcpy(ready_buffer + (i * bo_stride), src_row, 4 * curdata->w);
+        }
     }
 
     /* Dump the cursor buffer to our GBM BO. */
@@ -189,10 +198,18 @@ KMSDRM_DumpCursorToBO(SDL_VideoDisplay *display, SDL_Cursor *cursor)
     /* Put the GBM BO buffer on screen using the DRM interface. */
     bo_handle = KMSDRM_gbm_bo_get_handle(dispdata->cursor_bo).u32;
     if (curdata->hot_x == 0 && curdata->hot_y == 0) {
-        ret = KMSDRM_drmModeSetCursor(viddata->drm_fd, dispdata->crtc->crtc_id,
+        if (odroidgo_rotate)
+          ret = KMSDRM_drmModeSetCursor(viddata->drm_fd, dispdata->crtc->crtc_id,
+            bo_handle, dispdata->cursor_h, dispdata->cursor_w);
+        else
+          ret = KMSDRM_drmModeSetCursor(viddata->drm_fd, dispdata->crtc->crtc_id,
             bo_handle, dispdata->cursor_w, dispdata->cursor_h);
     } else {
-        ret = KMSDRM_drmModeSetCursor2(viddata->drm_fd, dispdata->crtc->crtc_id,
+        if (odroidgo_rotate)
+          ret = KMSDRM_drmModeSetCursor2(viddata->drm_fd, dispdata->crtc->crtc_id,
+            bo_handle, dispdata->cursor_h, dispdata->cursor_w, curdata->hot_y, (curdata->w - curdata->hot_x + 1));
+        else
+          ret = KMSDRM_drmModeSetCursor2(viddata->drm_fd, dispdata->crtc->crtc_id,
             bo_handle, dispdata->cursor_w, dispdata->cursor_h, curdata->hot_x, curdata->hot_y);
     }
 
@@ -378,9 +395,9 @@ KMSDRM_WarpMouseGlobal(int x, int y)
     SDL_Mouse *mouse = SDL_GetMouse();
 
     if (mouse && mouse->cur_cursor && mouse->focus) {
-
         SDL_Window *window = mouse->focus;
         SDL_DisplayData *dispdata = (SDL_DisplayData *) SDL_GetDisplayForWindow(window)->driverdata;
+        KMSDRM_CursorData *curdata = mouse->cur_cursor->driverdata;
 
         /* Update internal mouse position. */
         SDL_SendMouseMotion(mouse->focus, mouse->mouseID, 0, x, y);
@@ -389,7 +406,10 @@ KMSDRM_WarpMouseGlobal(int x, int y)
         if (dispdata->cursor_bo) {
             int ret = 0;
 
-            ret = KMSDRM_drmModeMoveCursor(dispdata->cursor_bo_drm_fd, dispdata->crtc->crtc_id, x, y);
+            if (odroidgo_rotate)
+                ret = KMSDRM_drmModeMoveCursor(dispdata->cursor_bo_drm_fd, dispdata->crtc->crtc_id, y, dispdata->mode.vdisplay + curdata->w - x);
+            else
+                ret = KMSDRM_drmModeMoveCursor(dispdata->cursor_bo_drm_fd, dispdata->crtc->crtc_id, x, y);
 
             if (ret) {
                 SDL_SetError("drmModeMoveCursor() failed.");
@@ -443,16 +463,19 @@ KMSDRM_MoveCursor(SDL_Cursor * cursor)
     /* We must NOT call SDL_SendMouseMotion() here or we will enter recursivity!
        That's why we move the cursor graphic ONLY. */
     if (mouse && mouse->cur_cursor && mouse->focus) {
-
         SDL_Window *window = mouse->focus;
         SDL_DisplayData *dispdata = (SDL_DisplayData *) SDL_GetDisplayForWindow(window)->driverdata;
+        KMSDRM_CursorData *curdata = mouse->cur_cursor->driverdata;
 
         if (!dispdata->cursor_bo) {
             SDL_SetError("Cursor not initialized properly.");
             return;
         }
 
-        ret = KMSDRM_drmModeMoveCursor(dispdata->cursor_bo_drm_fd, dispdata->crtc->crtc_id, mouse->x, mouse->y);
+        if (odroidgo_rotate)
+            ret = KMSDRM_drmModeMoveCursor(dispdata->cursor_bo_drm_fd, dispdata->crtc->crtc_id, mouse->y, dispdata->mode.vdisplay - curdata->w - mouse->x);
+        else
+            ret = KMSDRM_drmModeMoveCursor(dispdata->cursor_bo_drm_fd, dispdata->crtc->crtc_id, mouse->x, mouse->y);
 
         if (ret) {
             SDL_SetError("drmModeMoveCursor() failed.");
-- 
2.42.0

